# Copyright 2015 Volodymyr Medvid <vmedvid@riseup.net>
# Distributed under the terms of the GNU General Public License v2

require github [ user=equalsraf tag=v${PV} ] cmake
require freedesktop-desktop gtk-icon-cache
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test src_compile src_install pkg_postinst pkg_postrm

SUMMARY="Qt5 library and tools for interacting with Neovim."

LICENCES="ISC"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        dev-lang/python:*
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/msgpack[>=3.2.0]
        x11-libs/qtbase:5[>=5.8]
        x11-libs/qtsvg:5[>=5.8]
    run:
        app-editors/neovim
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DENABLE_CLAZY:BOOL=OFF
    -DENABLE_TIDY:BOOL=OFF
    -DUSE_GCOV:BOOL=OFF
    -DUSE_SYSTEM_MSGPACK:BOOL=ON
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'doc Doxygen'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DENABLE_TESTS:BOOL=TRUE -DENABLE_TESTS:BOOL=FALSE'
)

# Fails complaining about different monospace fonts
DEFAULT_SRC_TEST_PARAMS+=( -E tst_qsettings )

neovim-qt_src_test() {
    esandbox allow_net "unix:${TEMP%/}/**"
    esandbox allow_net "unix:${WORK}/test/*nvimsock"
    esandbox allow_net --connect "inet:127.0.0.1@1024-65535"

    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop

    esandbox allow_net --connect "unix-abstract:/tmp/.ICE-unix/*"
    esandbox disallow_net --connect "inet:127.0.0.1@1024-65535"
    esandbox disallow_net "unix:${WORK}/test/*nvimsock"
}

neovim-qt_src_compile() {
    default

    if option doc ; then
        emake doc
    fi
}

neovim-qt_src_install() {
    cmake_src_install

    if option doc ; then
        edo pushd "${WORK}"/doc
        dodoc -r html
        edo popd
    fi
}

neovim-qt_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

neovim-qt_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

