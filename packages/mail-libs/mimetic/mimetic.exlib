# Copyright 2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

export_exlib_phases src_compile src_install

SUMMARY="A full featured, STL-based, standards compliant C++ MIME library"
DESCRIPTION="
mimetic has been built around the C++ standard lib. This means that you'll not
find yet another string class or list implementation and that you'll feel
comfortable in using this library from the very first time.

Most lasses functionalities and behavior will be clear if you ever studied
MIME and its components; if you don't know anything about Internet messages
you'll probably want to read some RFCs to understand the topic and, therefore,
easily use the library whose names, whenever possible, overlap terms adopted
in the standard RFC documents. At the very least: RFC 822, RFC 2045 and
RFC 2046."

HOMEPAGE="https://www.codesink.org/mimetic_mime_library.html"
DOWNLOADS="https://www.codesink.org/download/${PNV}.tar.gz"

UPSTREAM_DOCUMENTATION="https://www.codesink.org/data/mimetic/docs/html/index.html"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen )
"

DEFAULT_SRC_CONFIGURE_PARAMS=( --disable-static )

mimetic_src_compile() {
    default

    option doc && emake -C doc docs
}

mimetic_src_install() {
    default

    if option doc ; then
        edo pushd doc
        dodoc -r html
        edo popd
    fi
}

