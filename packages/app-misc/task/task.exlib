# Copyright 2013 Maxime Coste <frrrwww@gmail.com>
# Copyright 2015-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

SCM_SECONDARY_REPOSITORIES="libshared"
SCM_EXTERNAL_REFS="src/libshared:libshared"
SCM_libshared_REPOSITORY="https://github.com/GothenburgBitFactory/libshared.git"

# master seems to be a release branch
SCM_BRANCH="2.6.0"

require github [ user=GothenburgBitFactory project=taskwarrior release=v${PV} suffix=tar.gz ]
require cmake bash-completion zsh-completion
require option-renames [ renames=[ 'gnutls sync' ] ]

export_exlib_phases src_prepare src_compile src_install

SUMMARY="Taskwarrior is a command-line todo list manager."
HOMEPAGE+=" https://taskwarrior.org/"

# Tests are not shipped with the tarball:
ever is_scm || DOWNLOADS+=" https://github.com/GothenburgBitFactory/taskwarrior/releases/download/v${PV}/tests-${PV}.tar.gz"

LICENCES="MIT"
SLOT="0"
MYOPTIONS="
    sync [[ description = [ Share tasks between devices and sync them with a server ] ]]"

DEPENDENCIES="
    build:
        sync? ( virtual/pkg-config   [[ note = [ FindGnuTLS in cmake ] ]] )
    build+run:
        sys-apps/util-linux
        sync? ( dev-libs/gnutls )
    test:
        dev-lang/python:*
"

CMAKE_SRC_CONFIGURE_OPTION_ENABLES=( SYNC )
CMAKE_SRC_CONFIGURE_PARAMS=(
    -DTASK_DOCDIR=/usr/share/task
    -DTASK_MAN1DIR=/usr/share/man/man1
    -DTASK_MAN5DIR=/usr/share/man/man5
    -DENABLE_WASM:BOOL=FALSE
)

task_src_prepare() {
    cmake_src_prepare

    edo mv "${WORKBASE}"/test "${CMAKE_SOURCE}"
}

task_src_compile() {
    default

    expecting_tests && emake build_tests
}

task_src_install() {
    cmake_src_install

    # Don't install docs already installed by emagicdocs into a wrong dir
    edo rm "${IMAGE}"/usr/share/task/{AUTHORS,ChangeLog,COPYING,INSTALL,LICENSE,NEWS,README.md}

    # Don't install zsh completions unconditionally - and into a wrong dir
    edo rm -r "${IMAGE}"/usr/$(exhost --target)/share/zsh
    edo rmdir "${IMAGE}"/usr/$(exhost --target)/share

    dozshcompletion "${CMAKE_SOURCE}"/scripts/zsh/_task
    dobashcompletion "${CMAKE_SOURCE}"/scripts/bash/task.sh
}

